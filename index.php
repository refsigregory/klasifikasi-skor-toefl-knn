<?php
require_once __DIR__ . '/config.php';

if ($mysqli -> connect_errno) {
    echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
    exit();
}

?>

<?php

$data = [];
$labels  = [];
$mahasiswa = [];

if(isset($_GET['import']))
{
    //$file = $_FILES["file"]["tmp_name"];
    $handle = fopen("data/insert_data.csv", "r");
    //$handle = fopen($file, "r");
    $index = 0;
    if($handle !== FALSE) {
      while (($row = fgetcsv($handle)) !== FALSE) {
          
          if($index>=1):
            $result = $mysqli -> query("select * from nilai_mahasiswa where id_mahasiswa = '".$row[0]."'");
            if($result->num_rows > 0) {
              $obj = $result -> fetch_object();
              $insert = $mysqli -> query("update nilai_mahasiswa set `".$_GET['import']."` = '".$row[2]."' WHERE id_mahasiswa = '".$obj->id_mahasiswa."'");
            }else {
              $insert = $mysqli -> query("insert into nilai_mahasiswa (`id_mahasiswa`, `nama_mahasiswa`, `".$_GET['import']."`) values('".$row[0]."', '".$row[1]."', '".$row[2]."')");
            }
            if(!$insert){
                die("Gagal di data ke " . $index . "(".$mysqli -> error.")");
            }
          endif;
          $index+=1;
      }
      fclose( $handle );
    }
}

if(isset($_GET['train']))
{
    //$file = $_FILES["file"]["tmp_name"];
    $handle = fopen("data/data_train_small.csv", "r");
    //$handle = fopen($file, "r");
    $index = 0;
    if($handle !== FALSE) {
      while (($row = fgetcsv($handle)) !== FALSE) {
          if($index>=0):
            //$data[] = $row;
            //$datatrain[] = [$row[1], $row[2], $row[3], $row[4], $row[5], $row[6]];
            //$labels[] = $row[7];
            $insert = $mysqli -> query("insert into nilai_mahasiswa values('".$index."', '".$row[0]."','".$row[1]."','".$row[2]."','".$row[3]."','".$row[4]."','".$row[5]."')");
            if(!$insert){
                die("Gagal di data ke " . $index . "(".$mysqli -> error.")");
            }
          endif;
          $index+=1;
      }
      fclose( $handle );
    }
}

/* Proses Prediksi */
if(isset($_GET['process'])){

class KNN
{
    use Train;
    use Predict;
    var $k;
    var $distanceMetric;

    public function __construct(int $k = 3, ?Distance $distanceMetric = null)
    {
        if ($distanceMetric === null) {
            $distanceMetric = new Euclidean(); /* Hitung Euclidan */
        }

        $this->k = $k;
        $this->samples = [];
        $this->targets = [];
        $this->distanceMetric = $distanceMetric;
    }

    public function predictSample(array $sample)
    {
        $distances = $this->hitungKNN($sample);
        $predictions = (array) array_combine(array_values($this->targets), array_fill(0, count($this->targets), 0));

        foreach (array_keys($distances) as $index) {
            ++$predictions[$this->targets[$index]];
        }

        arsort($predictions);
        reset($predictions);

        return key($predictions);
    }

    public function hitungKNN(array $sample): array
    {
        $distances = [];

        foreach ($this->samples as $index => $neighbor) {
            $distances[$index] = $this->distanceMetric->distance($sample, $neighbor);
        }

        asort($distances);

        return array_slice($distances, 0, $this->k, true);
    }
}

    $mahasiswa[] = [$_POST['nama'], $_POST['attr_1'], $_POST['attr_2'], $_POST['attr_3'], $_POST['attr_4'], $_POST['attr_5'], $_POST['attr_6']];

    if(isset($_POST['nama']) || isset($_POST['attr_1']) || isset($_POST['attr_2']) || isset($_POST['attr_3']) || isset($_POST['attr_4'])) {
      } else {
        echo "<script>alert('Harap memasukkan data');window.history.back();</script>";
      }

      if($_POST['nama'] == "" || $_POST['attr_1'] == "" || $_POST['attr_2'] == "" || $_POST['attr_3'] == "" || $_POST['attr_4'] == "") {
        echo "<script>alert('Data tidak boleh kosong');window.history.back();</script>";
      }

    $sql = "SELECT * FROM nilai_mahasiswa ORDER BY id_mahasiswa";

    if ($result = $mysqli -> query($sql)) {
    while ($obj = $result -> fetch_object()) {
        //printf("%s (%s)\n", $obj->Lastname, $obj->Age);
        $datatrain[] = [$obj->nilai_bhs_ing1, $obj->nilai_bhs_ing2, $obj->nilai_bhs_ing3, $obj->nilai_bhs_ing4];
        $labels[] = $obj->nilai_toefl;
        $data[] = [$obj->nama, $obj->nilai_bhs_ing1, $obj->nilai_bhs_ing2, $obj->nilai_bhs_ing3, $obj->nilai_bhs_ing4,$obj->nilai_toefl, (sqrt(pow(floatval($_POST['attr_1'])-$obj->nilai_bhs_ing1,2)+pow(floatval($_POST['attr_2'])-$obj->nilai_bhs_ing2,2)+pow(floatval($_POST['attr_3'])-$obj->nilai_bhs_ing3,2)+pow(floatval($_POST['attr_4'])-$obj->nilai_bhs_ing4,2)))];
        $e_data[] = (sqrt(pow(floatval($_POST['attr_1'])-$obj->nilai_bhs_ing1,2)+pow(floatval($_POST['attr_2'])-$obj->nilai_bhs_ing2,2)+pow(floatval($_POST['attr_3'])-$obj->nilai_bhs_ing3,2)+pow(floatval($_POST['attr_4'])-$obj->nilai_bhs_ing4,2)));
    }
    $result -> free_result();
    }

    $mysqli -> close();
    
    $_KNN = new KNN($k=3);
    $_KNN->train($datatrain, $labels);

    $knn = $_KNN->predict([$_POST['attr_1'],$_POST['attr_2'],$_POST['attr_3'],$_POST['attr_4']]);
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Klasifikasi Skor TOEFL</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div class="row">
      <div class="col">
        <h1 style="text-align:center">Aplikasi Klasifikasi Score TOEFL Mahasiswa</h1>
      </div>
    </div>
<?php
if(!isset($_GET['process'])):
?>
  <div class="row">
      <div class="col">
        <form action="?process=start" enctype="multipart/form-data" method="POST">
              <div class="form-group">
                  <label>Nama : </label>
                  <br>
                  <input type="text" class="form-control" name="nama">
              </div>
              <div class="form-group">
                  <label>Nilai Bahasa Inggris I : </label>
                  <br>
                  <input type="number" class="form-control" name="attr_1" min="55" max="100">
              </div>
              <div class="form-group">
                  <label>Nilai Bahasa Inggris II : </label>
                  <br>
                  <input type="number" class="form-control" name="attr_2" min="55" max="100">
              </div>
              <div class="form-group">
                  <label>Nilai Bahasa Inggris III : </label>
                  <br>
                  <input type="number" class="form-control" name="attr_3" min="55" max="100">
              </div>
              <div class="form-group">
                  <label>Nilai Bahasa Inggris IV : </label>
                  <br>
                  <input type="number" class="form-control" name="attr_4" min="55" max="100">
              </div>
              <button type="submit" class="btn btn-primary btn-block">PROSES</button>
          </form>
      </div>
  </div>
<?php endif;?>

<?php

if(isset($_GET['process'])):
?>
<div class="row">
    <div class="col">
      <div class="">
        <div class="card-body">
        <a href="/" class="btn btn-primary btn-block">RESET</a>
        <br>
        <h2>Hasil</h2>
            <?php

if(isset($mahasiswa)):
echo "<table class='table table-bordered'>";
echo "<tr><th>Nama</th><th>Nilai Bahasa Inggris I</th><th>Nilai Bahasa Inggris II</th><th>Nilai Bahasa Inggris III</th><th>Nilai Bahasa Inggris IV</th><th>Skor TOEFL</th></tr>";
foreach($mahasiswa as $row)
{

        echo "<tr>";
        echo "<td>".$row[0]."</td>";
        echo "<td>".$row[1]."</td>";
        echo "<td>".$row[2]."</td>";
        echo "<td>".$row[3]."</td>";
        echo "<td>".$row[4]."</td>";
        echo "<td align='center'><b style='color: ". ($knn <= 420 ? 'red' : 'green') . "'>" . $knn. "</b><br><small>". ($knn <= 420 ? 'TIDAK ' : ' ') . "LULUS</small>" ."</td>";
        echo "</tr>";
}
echo "</table>";
endif;
            ?>
   
        </div>
      </div>
    </div>
  </div>
<?php
  if(isset($data)):
?>
  <div class="row">
    <div class="col">
      <div class="">
        <div class="card-body">
        <h2>Data Train:</h2>
            <?php
$no = 1;
echo "<table class='table table-bordered'>";
echo "<tr><th>Ranking</th><th>Nilai Bahasa Inggris I</th><th>Nilai Bahasa Inggris II</th><th>Nilai Bahasa Inggris III</th><th>Nilai Bahasa Inggris IV</th><th>Skor TOEFL</th><th>Eculidean
</th></tr>";
function cmp($a, $b)
{
    return strcmp($a[6], $b[6]);
}

asort($e_data);

foreach($e_data as $idx=>$row)
{

        if($no <= 3):
          echo "<tr style='background: #ff2222'>";
        else:
          echo "<tr>";
        endif;

        echo "<td>".$no++."</td>";
        echo "<td>".$data[$idx][1]."</td>";
        echo "<td>".$data[$idx][2]."</td>";
        echo "<td>".$data[$idx][3]."</td>";
        echo "<td>".$data[$idx][4]."</td>";
        echo "<td>".$data[$idx][5]."</td>";
        echo "<td>".$data[$idx][6]."</td>";
        echo "</tr>";
}
echo "</table>";
            ?>
   
        </div>
      </div>
    </div>
<?php endif; ?>
    
<?php endif; ?>

    </div>
  </div>
</div>

</body>
</html>



