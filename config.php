<?php 
/* Database */
$mysqli = new mysqli("localhost","root","","ta_prediksi_toefl");

trait Predict
{
    /**
     * @return mixed
     */
    public function predict(array $samples)
    {
        if (!is_array($samples[0])) {
            return $this->predictSample($samples);
        }

        $predicted = [];
        foreach ($samples as $index => $sample) {
            $predicted[$index] = $this->predictSample($sample);
        }

        return $predicted;
    }

    /**
     * @return mixed
     */
    abstract protected function predictSample(array $sample);
}


trait Train
{
    /**
     * @var array
     */
    private $samples = [];

    /**
     * @var array
     */
    private $targets = [];

    public function train(array $samples, array $targets): void
    {
        $this->samples = array_merge($this->samples, $samples);
        $this->targets = array_merge($this->targets, $targets);
    }
}


abstract class Distance
{
    public $norm;

    public function __construct(float $norm = 3.0)
    {
        $this->norm = $norm;
    }

    public function distance(array $a, array $b): float
    {
        $distance = 0;

        foreach ($this->deltas($a, $b) as $delta) {
            $distance += $delta ** $this->norm;
        }

        return $distance ** (1 / $this->norm);
    }

    protected function deltas(array $a, array $b): array
    {
        $count = count($a);

        if ($count !== count($b)) {
            echo 'Size of given arrays does not match';
        }

        $deltas = [];

        for ($i = 0; $i < $count; $i++) {
            $deltas[] = abs($a[$i] - $b[$i]);
        }

        return $deltas;
    }
}

class Euclidean extends Distance
{
    public function __construct()
    {
        parent::__construct(2.0);
    }
    public function sqDistance(array $a, array $b): float
    {
        return $this->distance($a, $b) ** 2;
    }
}